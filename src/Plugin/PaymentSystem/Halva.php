<?php

declare(strict_types=1);

namespace Drupal\site_payments_sovcombank\Plugin\PaymentSystem;

use Drupal\Core\Url;
use Drupal\Core\Utility\Error;
use Drupal\site_payments\PaymentSystemPluginBase;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Drupal\site_payments\TransactionInterface;
use Drupal\kvantstudio\Validator;
use SimpleXMLElement;

/**
 * @PaymentSystem(
 *   id="site_payments_sovcombank_halva",
 *   label = @Translation("Halva (Sovcombank)"),
 *   payment_method_name = @Translation("Halva (Sovcombank)")
 * )
 */
class Halva extends PaymentSystemPluginBase {

  private const HOST_PROD = 'https://pay.best2pay.net/webapi';
  private const HOST_TEST = 'https://test.best2pay.net/webapi';

  /**
   * {@inheritdoc}
   */
  public function getPaymentSettings(?int $payment_account_id = NULL): array {
    $values = \Drupal::state()->getMultiple([
      'site_payments.allow_test_payment',
      'site_payments.test_hostnames',
      'site_payments.test_usernames',
      'site_payments_sovcombank.block_rules'
    ]);

    $array_state = [
      'allow_test_payment' => (bool) ($values['site_payments.allow_test_payment'] ?? FALSE),
      'test_hostnames' => $values['site_payments.test_hostnames'] ?? NULL,
      'test_usernames' => $values['site_payments.test_usernames'] ?? NULL,
      'block_rules' => (int) ($values['site_payments_sovcombank.block_rules'] ?? 0),
    ];

    $array_account = $this->getAccount($payment_account_id);
    if ($array_account) {
      return array_merge($array_state, $array_account);
    }

    return $array_state;
  }

  /**
   * {@inheritdoc}
   */
  public function getAvailableStatus(): bool {
    $settings = $this->getPaymentSettings();
    return (bool) ($settings['status'] ?? FALSE);
  }

  /**
   * {@inheritdoc}
   */
  public function getRulesPaymentSystem(): array {
    $settings = $this->getPaymentSettings();

    $build = [];
    if ($settings['block_rules_bnlp']) {
      $entity_type = 'block_content';
      $view_mode = 'default';
      $block = $this->entityTypeManager->getStorage($entity_type)->load($settings['block_rules_bnlp']);
      $view_builder = $this->entityTypeManager->getViewBuilder($entity_type);
      $build = $view_builder->view($block, $view_mode);
    }

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getWebhookUrl(): string {
    $route_name = 'site_payments_sovcombank.payment_webhook';
    $url = Url::fromRoute($route_name);

    return $url->setAbsolute()->toString();
  }

  /**
   * {@inheritdoc}
   */
  public function getOrderId(TransactionInterface $transaction): int|string {
    $order_id = $transaction->getOrderId();
    $created = $transaction->getCreatedTime();

    return "{$order_id}-{$created}";
  }

  /**
   * {@inheritdoc}
   */
  public function getVatType(?string $type, string $default_type = 'NONE'): int|string {
    $values = [
      'NONE' => 6,
      '0' => 5,
      '10' => 2,
      '110' => 4,
      '20' => 1,
      '120' => 3,
    ];

    if (empty($type)) {
      return $values[$default_type];
    }

    return $values[$type] ?? $values[$default_type];
  }

  /**
   * {@inheritdoc}
   */
  public function getPaymentUrl(array $payment_data): string {
    // Link for payment URL page.
    $payment_url = '';

    // We determine which bank account to choose in the payment system.
    $payment_account_id = empty($payment_data['payment_account']) ? NULL : (int) $payment_data['payment_account'];

    // Payment system settings for a specific bank account.
    $config = $this->getPaymentSettings($payment_account_id);
    $payment_data['payment_account'] = $config['id'];

    // Expiration time of the payment link.
    $payment_data['overdue'] = (int) (empty($config['overdue']) ? 2592000 : ($config['overdue'] * 3600));

    // The name of the payment plugin.
    if (empty($payment_data['payment_plugin'])) {
      $payment_data['payment_plugin'] = $this->getId();
    }

    // The name of the payment system.
    if (empty($payment_data['payment_system'])) {
      $payment_data['payment_system'] = $config['payment_system'];
    }

    // The cost must be rounded to two decimal places after the separator.
    $payment_data['price'] = round((float) $payment_data['price'], 2);

    // Register a transaction.
    $transaction = $this->createTransaction($payment_data, TRUE);
    if (!$transaction) {
      $error_data = json_encode($payment_data, JSON_UNESCAPED_UNICODE);
      $this->logger->error("Transaction not created: {$error_data}.");
      return $payment_url;
    }

    // Change the price for transaction.
    $payment_data['price'] = $this->correctPaidNumber($transaction, $config);

    // Register receipt.
    $this->createReceipt($transaction);

    // Check if the cost exists, that it is greater than zero.
    $payment = [];
    if (Validator::comparingNumbers($payment_data['price'])) {
      $params = [
        'is_test_payment' => $config['is_test_payment'],
        'description' => $payment_data['data']['description'],
        'address' => '',
        'transaction' => $transaction,
        'price' => $payment_data['price'],
        'success_redirect' => empty($payment_data['success_redirect_route']) ? $this->getSuccessUrl($transaction->uuid()) : $this->getSuccessUrl($transaction->uuid(), $payment_data['success_redirect_route']),
        'failure_redirect' => empty($payment_data['failure_redirect_route']) ? $this->getFailureUrl($transaction->uuid()) : $this->getFailureUrl($transaction->uuid(), $payment_data['failure_redirect_route']),
        'overdue' => $payment_data['overdue'],
        'token' => $config['token'],
        'merchant' => $config['merchant'],
        'mode' => $config['mode'],
        'verify_ssl' => $config['verify_ssl'],
        'fiscal' => $config['fiscal'],
        'vat_type' => $config['vat_type']
      ];
      $payment = $this->registerOrder($params);
    } else {
      $error_data = json_encode($payment_data, JSON_UNESCAPED_UNICODE);
      $this->logger->error("Order price must be greater than zero: {$error_data}.");
    }

    // Get a link to pay from the result of the request.
    if ($payment) {
      $payment_url = $payment['link'];

      // Assign the data received from the payment system to the transaction.
      $transaction->setPaymentLink($payment['link']);
      $transaction->setPaymentStatus('issued');
      $transaction->save();
    } else {
      $this->logger->error("Error while generate payment URL.");
    }

    return $payment_url;
  }

  /**
   * Creates an order in the payment system.
   *
   * @param array $data
   * @return array|null
   */
  private function registerOrder(array $data): ?array {
    $payment = NULL;

    try {
      /** @var \Drupal\site_payments\TransactionInterface $transaction */
      $transaction = $data['transaction'];

      // An order amount.
      $amount = $data['price'] * 100;
      $currency = 643;

      // Password for digital signature calculation.
      $password = $data['token'];

      // Generate signature for request.
      $str = "{$data['merchant']}{$amount}{$currency}{$password}";
      $str = md5($str);
      $signature = base64_encode($str);

      $client = new Client();
      $host = $data['mode'] ? self::HOST_PROD : self::HOST_TEST;

      $order_id = $transaction->getOrderId();
      $life_period = $data['overdue'];

      // Data for order register.
      $form_params = [
        'sector' => $data['merchant'],
        'amount' => $amount,
        'currency' => $currency,
        'address' => $data['address'],
        'description' => $data['description'],
        'signature' => $signature,
        'reference' => $order_id,
        'life_period' => $life_period,
        'url' => $data['success_redirect'],
        'failurl' => $data['failure_redirect'],
      ];

      /** @var \Drupal\site_commerce_cart\CartStorageInterface $storage_cart */
      $storage_cart = $this->entityTypeManager->getStorage('site_commerce_cart');
      $shop_cart = [];
      $items = $storage_cart->getItemsByOrderId($order_id);
      foreach ($items as $item) {
        $value = round((float) $item['paid__number'], 2);
        $value = number_format($value, 2, '.', '');
        $shop_cart[] = [
          'name' => $item['title'],
          'goodCost' => $value,
          'quantityGoods' => (string) $item['quantity']
        ];
      }

      // Add fiscal data to registration process.
      if ($data['fiscal'] == 'registration') {
        $vat_type = self::getVatType($data['vat_type']);
        $fiscal_items = [];
        if ($data['is_test_payment']) {
          $fiscal_items[] = implode(";", [
            1,
            $amount,
            $vat_type,
            mb_strimwidth($data['description'], 0, 128, "...")
          ]);
        } else {
          foreach ($items as $item) {
            $value = round((float) $item['paid__number'], 2);
            $value = $value * 100;
            $fiscal_items[] = implode(";", [
              (int) $item['quantity'],
              $value,
              $vat_type,
              mb_strimwidth($item['title'], 0, 128, "...")
            ]);
          }
        }

        $form_params['fiscal_positions'] = implode("|", $fiscal_items);
      }

      try {
        $url = "$host/Register";
        $request = $client->post($url, [
          'verify' => $data['verify_ssl'],
          'form_params' => $form_params,
          'headers' => [
            'Content-Type' => 'application/x-www-form-urlencoded',
          ],
        ]);
        $content = $request->getBody()->getContents();

        $document = new SimpleXMLElement($content);
        if ($document instanceof SimpleXMLElement) {
          $id = (int) $document->id ?? 0;
          $state = $document->state ?? '';
          $inprogress = (int) $document->inprogress ?? 0;
          $code = $document->code ?? 0;

          // Save payment id.
          $transaction->setPaymentId((string) $id);
          $transaction->save();

          if ($state == 'REGISTERED' && !$inprogress && !$code) {
            // Create cart parametr.
            $shop_cart_json = json_encode($shop_cart, JSON_UNESCAPED_UNICODE);
            $shop_cart_base64 = base64_encode("$shop_cart_json");

            // Generate signature for request.
            $str = "{$data['merchant']}{$id}{$shop_cart_base64}{$password}";
            $str = md5($str);
            $signature = base64_encode($str);

            // Clean parametrs.
            $signature = str_replace(['+'], ['%2B'], $signature);
            $shop_cart_base64 = str_replace(['+'], ['%2B'], $shop_cart_base64);

            $payment['link'] = "$host/custom/svkb/PurchaseWithInstallment?signature={$signature}&sector={$data['merchant']}&id={$id}&shop_cart={$shop_cart_base64}";
          }
        }
      } catch (RequestException $e) {
        Error::logException($this->logger, $e);
      }
    } catch (\Exception $e) {
      Error::logException($this->logger, $e);
    }

    return $payment;
  }

  /**
   * {@inheritdoc}
   */
  function checkPaymentStatus(TransactionInterface $transaction, array $settings = []): ?string {
    // Payment system settings.
    $payment_account = $transaction->getPaymentAccountId();
    $config = array_merge($this->getPaymentSettings($payment_account), $settings);

    $result = NULL;

    try {
      $client = new Client();
      $host = $config['mode'] ? self::HOST_PROD : self::HOST_TEST;

      $data = $transaction->getData();
      $payment_data = $data['payment_system'] ?? NULL;
      if (!$payment_data) {
        return $result;
      }

      // Password for digital signature calculation.
      $password = $config['token'];

      // Generate signature for request.
      $str = "{$config['merchant']}{$payment_data['id']}{$payment_data['operation']}{$password}";
      $str = md5($str);
      $signature = base64_encode($str);

      $url = "$host/Operation";
      $request = $client->post($url, [
        'verify' => $config['verify_ssl'],
        'form_params' => [
          'sector' => $config['merchant'],
          'id' => $payment_data['id'],
          'operation' => $payment_data['operation'],
          'signature' => $signature
        ],
        'headers' => [
          'Content-Type' => 'application/x-www-form-urlencoded',
        ],
      ]);
      $content = $request->getBody()->getContents();

      $document = new SimpleXMLElement($content);
      if ($document instanceof SimpleXMLElement) {
        $order_state = $document->order_state ?? '';
        $state = $document->state ?? '';
        if ($order_state == 'COMPLETED' && $state == 'APPROVED') {
          $result = 'paid';
        }
      }
    } catch (\Exception $e) {
      Error::logException($this->logger, $e);
    }

    return $result;
  }
}
