<?php

namespace Drupal\site_payments_sovcombank\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\State\StateInterface;

/**
 * Class Form of sovcombank settings.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The Sate API object.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   */
  public function __construct(ConfigFactoryInterface $config_factory, StateInterface $state) {
    parent::__construct($config_factory);
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('state')
    );
  }

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'site_payments_sovcombank_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'site_payments_sovcombank.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $block_rules = (int) $this->state->get('site_payments_sovcombank.block_rules');
    $block = \Drupal::entityTypeManager()->getStorage('block_content')->load($block_rules);
    $form['block_rules'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Rules for the use of the payment system'),
      '#target_type' => 'block_content',
      '#default_value' => $block,
      '#description' => $this->t('You need to create a custom block and select it in this field. The block content is displayed under the select payment method field.')
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    // Правила использования платежной системы.
    $block_rules = trim($form_state->getValue('block_rules'));
    $this->state->set('site_payments_sovcombank.block_rules', $block_rules);
  }
}
